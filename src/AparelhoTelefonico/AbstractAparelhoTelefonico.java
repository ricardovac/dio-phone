package AparelhoTelefonico;


public abstract class AbstractAparelhoTelefonico implements AparelhoTelefonico {
    public void makeCall() {
        System.out.println("Fazendo uma chamada");
    }

    public void receiveCall() {
        System.out.println("Recebendo uma chamada");
    }

    public void endCall() {
        System.out.println("Finalizando uma chamada");
    }

    public void sendTextMessage() {
        System.out.println("Enviando uma mensagem de texto");
    }
}
