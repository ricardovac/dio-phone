package AparelhoTelefonico;

public interface AparelhoTelefonico {
    void fazerLigacao();
    void receberLigacao();
    void sairDaLigacao();
    void mandarMensagem();
}
