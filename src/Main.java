import AparelhoTelefonico.iPhoneTelefonico;
import NavegadorInternet.iPhoneInternet;
import ReprodutorMusical.iPhoneMusical;

public class Main {
    public static void main(String[] args) {
        iPhoneMusical iPhoneMusical = new iPhoneMusical();
        iPhoneTelefonico iPhoneTelefonico = new iPhoneTelefonico();
        iPhoneInternet iPhoneInternet = new iPhoneInternet();

        System.out.println("Testando iPhone como Reprodutor Musical:");
        iPhoneMusical.play();
        iPhoneMusical.pause();
        iPhoneMusical.nextTrack();
        iPhoneMusical.previousTrack();

        System.out.println("\nTestando iPhone como Aparelho Telefônico:");
        iPhoneTelefonico.makeCall();
        iPhoneTelefonico.receiveCall();
        iPhoneTelefonico.endCall();
        iPhoneTelefonico.sendTextMessage();

        System.out.println("\nTestando iPhone como Navegador de Internet:");
        iPhoneInternet.navigateToURL("www.google.com");
        iPhoneInternet.refreshPage();
        iPhoneInternet.goBack();
        iPhoneInternet.goForward();
    }
}