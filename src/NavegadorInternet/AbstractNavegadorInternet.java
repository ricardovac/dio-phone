package NavegadorInternet;
public abstract class AbstractNavegadorInternet implements NavegadorInternet {
    public void navigateToURL(String url) {
        System.out.println("Navegando para " + url);
    }

    public void refreshPage() {
        System.out.println("Atualizando a página");
    }

    public void goBack() {
        System.out.println("Voltando para a página anterior");
    }

    public void goForward() {
        System.out.println("Avançando para a próxima página");
    }
}
