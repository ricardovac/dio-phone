package NavegadorInternet;

public interface NavegadorInternet {
    void navegarParaURL(String url);
    void voltarPagina();
    void avancarPagina();
    void atualizarPagina();
}
