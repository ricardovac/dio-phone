package ReprodutorMusical;


public abstract class AbstractReprodutorMusical implements ReprodutorMusical {
    public void play() {
        System.out.println("Tocando música");
    }

    public void pause() {
        System.out.println("Música pausada");
    }

    public void nextTrack() {
        System.out.println("Próxima faixa");
    }

    public void previousTrack() {
        System.out.println("Faixa anterior");
    }
}
