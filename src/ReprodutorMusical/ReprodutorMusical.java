package ReprodutorMusical;

public interface ReprodutorMusical {
    void tocar();
    void pausar();
    void proximaMusica();
    void musicaAnterior();
}
